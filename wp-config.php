<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'indiahaat' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#p7*F+Oo*+Y]]XdMf}q5uP4Qu #xtXYRkh1_X8EP!YwFM^8l%$QXpa<qN8g*l^CF' );
define( 'SECURE_AUTH_KEY',  'aK31#`T4|X3mtr+:3HlAVwLiGFc(rSYv&dcI2j(qBswNkF5Mwr8|Zc_E[N^{[IQf' );
define( 'LOGGED_IN_KEY',    'r6Q*k3i- *&9tP=T2EwITA>[Je;?tIDO6t(_[hA !H,|+9I}+/qb(kASh76gbo6~' );
define( 'NONCE_KEY',        '7-+.YM<bOf.X<XT]PZI+s$S_*qmd@P?@8w|J?P[x;.O)RT!N^SI[+.xfpq7C3iYy' );
define( 'AUTH_SALT',        '8^n}f1-0!w(mw*OnK=:0RYvy5<:{Gt2Xy~S~.~,F5AHF:tswcMm5BM{[V0zta)F4' );
define( 'SECURE_AUTH_SALT', 'iF$=U#@&,zA8w]l;._KV/x/P2y#]>RxBJ68ZZ rlMr>Zw=)-2Q64`,.*<9IEKA(|' );
define( 'LOGGED_IN_SALT',   'K4x_NtBiW$mq4vl|lr5#c+`)M(;t+10r&Ku]>]oiPyd(<#R2iUUz(P@|k5TKri.4' );
define( 'NONCE_SALT',       '<L]AWh5b@=oTm1Uc@+*~(@Wp|,5j,jgQn$?`SQ)czH/J^;=EXv~}30,S=tQKZ^I%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpks11_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
