<?php global $post;?>
<?php include( locate_template( 'templates/page-layout.php' ) ); ?>
<?php if(is_page('verify-email')){ ?>
<script>
jQuery(document).ready(function(){
	var user = getUrlVars()["u"];
	var token = getUrlVars()["t"];
	var dataString = {'user': user,'token': token };
	jQuery.ajax({
			type: "POST",
			url: "http://iam.api.india-haat.com/user/mutations/verify-email",
			data: dataString,
			success: function(data) {
			   jQuery("#loader").hide();
			   if(data.statusCode == 200){ jQuery("#email-verify").show(); jQuery("#email-error").hide();}
			   else { jQuery("#email-verify").hide(); jQuery("#email-error").show();}
			},
			error: function (jqXHR, exception) {
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connected.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				console.log(msg);
				jQuery("#email-verify").hide(); jQuery("#email-error").show();
			}
		  });
});
function getUrlVars()
{
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for(var i = 0; i < hashes.length; i++)
	{
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}
</script>
<?php }?>


<div class="inner-container">
	<?php include( locate_template( 'templates/page-header.php' ) ); // Page Header Template ?>

	<?php echo wp_kses_post($outer_container_open) . wp_kses_post($outer_row_open); // Outer Tag Open ?>

    <?php /* OPEN MAIN CLASS */
    echo wp_kses_post($main_class_open); // support for sidebar ?>
    <?php get_template_part('templates/content', 'page'); ?>
    
    <?php // check if sidebar and remove container, else leave it. ?>
    <!-- Comment form for pages -->
	<?php echo wp_kses_post($inner_container_open); ?>
        <div class="row">
			<div class="col-md-12">
	        <?php comments_template('/templates/comments.php'); ?>
            </div>
        </div>
    <?php echo wp_kses_post($inner_container_close); ?>
    <!-- End Comment form for pages -->
    
    <?php 
    /* CLOSE MAIN CLASS */
    echo wp_kses_post($main_class_close); ?>
    
    <?php
    /* SIDEBAR */
    include themo_sidebar_path(); ?>              
    
    <?php 
    echo wp_kses_post($outer_container_close) . wp_kses_post($outer_row_close); // Outer Tag Close ?>
</div><!-- /.inner-container -->