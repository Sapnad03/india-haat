<?php /* Template Name: Register Form */ ?>
<style>
.overlay_style{
	background-color: #FFFFFF; box-shadow: 0px 0px 34px 0px rgba(0, 0, 0, 0.14);
    transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
    margin: -145px 0px 0px 0px;
    padding: 50px 30px 15px 30px;
}
.frm-show-form label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.frm-show-form input.error, .frm-show-form textarea.error {
  border: 1px solid;
  border-color: #a94442;
}
</style>
<?php global $post; ?>
<?php include (locate_template('templates/page-layout.php')); ?>
<?php
$th_no_sidebar_class = ' th-no-sidebar';
if (isset($has_sidebar) && $has_sidebar)
{
    $th_no_sidebar_class = false;
}
?>
<div class="inner-container<?php echo esc_attr($th_no_sidebar_class); ?>">
<?php include (locate_template('templates/page-header.php')); // Page Header Template
 ?>
	<section class="content-editor<?php echo esc_attr($th_empty_class); ?>">
	<div id="loader" style="display:none;"></div>

		<div class="elementor-widget-themo-formidable-form">
			<div class="elementor-widget-container">
			<div class="overlay_style" >
			
				<h1 class="title" style="color:#df672b;text-align:center;font-size:40px;">Registration</h1>
				<div class="alert alert-success" style="display:none; text-align:center;margin-top:30px;">
				  <strong>Success!</strong> Indicates a successful or positive action.
				</div>
				<div class="alert alert-danger" style="display:none; text-align:center;margin-top:30px;">
				  <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
				</div>
				<div class="th-fo-form  th-form-stacked  th-btn-form btn-standard-dark-form">
					<div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_3_container">
					
						<form enctype="multipart/form-data" method="post" class="frm-show-form" action="" name="registration">
						<!--div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<!--select type="text" value="" name="role"  placeholder="Role" disabled>
							<option value="wholesaler" selected>WHOLESALER </option>
							</select>
						</div-->
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="text" value="" name="firstName" placeholder="First Name" id="firstName">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="text" value="" name="lastName" placeholder="Last Name" id="lastName">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="text" value="" name="businessName" placeholder="Business Name"  id="businessName">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full" >
							<input type="text" value="" name="urlName" placeholder="URL" id="urlName">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<textarea  value="" name="address" placeholder="Address"  id="address"></textarea>
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="email" value="" name="email" placeholder="Email" id="email">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="password" value="" name="password" placeholder="Password" id="password">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							<input type="password" value="" name="confirm_password" placeholder="Confirm Password" id="confirm_password">
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full" >
							<div class="row">
								<div class="col-sm-2">
									<input type="number" value="91" name="countryCode" placeholder="91" disabled id="countryCode">
								</div>
								<div class="col-sm-10">
									<input type="number" value="" name="phonenumber" placeholder="Phone Number" id="phonenumber">
								</div>
							</div>
						</div>
						<div class="frm_form_field form-field  frm_required_field frm_none_container frm_full">
							
						</div>
						<div class="frm_submit">
							<input type="submit" value="Register">
						</div>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div><!-- /.inner-container -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery(".inner-container.th-no-sidebar .entry-title").text('');
});
jQuery(function() {
  jQuery("form[name='registration']").validate({
    rules: {
      // on the right side
      firstName: "required",
      lastName: "required",
	  businessName: "required",
	  phonenumber: "required",
	  address: "required",
	  urlName: "required",
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
	  confirm_password: {
		required: true,
        minlength: 6,
		equalTo: "#password"
	   },
	  
	  phonenumber:{
		required:true,
		minlength:10,
		maxlength:10,
      }
    },
    messages: {
      firstname: "Please enter your firstname",
      lastname: "Please enter your lastname",
      businessName: "Please enter your business name",
      address: "Please enter your address",
	  urlName: "This will be the url for accessing your portal",
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
	  confirm_password: "Enter Confirm Password Same as Password",
      email: "Please enter a valid email address",
	  phonenumber: "Please enter your 10 digit number"
    },
	
    submitHandler: function(form) {
		jQuery("#loader").show();
		//var role = jQuery("#role").val();
		var firstName = jQuery("#firstName").val();
		var lastName = jQuery("#lastName").val();
		var businessName = jQuery("#businessName").val();
		var urlName = jQuery("#urlName").val();
		var address = jQuery("#address").val();
		var password = jQuery("#password").val();
		var confirm_password = jQuery("#confirm_password").val();
		var countryCode = jQuery("#countryCode").val();
		var email = jQuery("#email").val();
		var phonenumber = jQuery("#phonenumber").val();
		var dataString = {'role': 'WHOLESALER','firstName': firstName,'email': email,'phoneNumber': phonenumber,'businessName': businessName,'urlName': urlName,'address': address,'password': password,'countryCode': countryCode};
		  //alert (dataString);return false;
		  jQuery.ajax({
			type: "POST",
			url: "http://iam.api.india-haat.com/user/mutations/signup",
			data: dataString,
			success: function(data) {
			   jQuery("#loader").hide();
			   form.reset();
			   if(data.statusCode == 201){ jQuery(".alert-success").show(); jQuery(".alert-success").html('Hurray! Your account is created successfully. Visit your shop <a href="'+data.data.user.wholesalerUrl+'">'+ data.data.user.wholesalerUrl+ '</a>' ); }
			   else { jQuery(".alert-danger").show(); jQuery(".alert-danger").text(data.message);}
			},
			error: function (jqXHR, exception) {
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connected.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				
				jQuery(".alert-danger").show(); jQuery(".alert-danger").text(msg);
			}
		  });
		  return false;
	 }
  });
});
</script>
